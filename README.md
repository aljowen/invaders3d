Invaders3d

Can be opened in visual studio, all linking is done using relative file paths, therefore it should "Just work" on any Windows PC.


This project was created for a university module, therefore it has some features which are not release build ready (which were required to hit grade criteria), such as performance logging every frame to the console, which is not performant.

One major improvement that could be made to the software would be how asset management is handled. As it currently stands, all assetts are loaded from file in realtime as they are created. This causes significant delay on respawning the alien ships at the end of each round, it would be much better if GameObjects used pointers to assets stored in memory. This would be far quicker and make the respawn times more or less instant.
