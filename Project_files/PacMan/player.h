#pragma once

#include "glew.h"
#include "SDL.h"
#include "SDL_opengl.h"

#include "glm.hpp"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <vector>

#include "2d.h"
#include "gameObject.h"
#include "bindings.h"
#include "missile.h"
#include "life.h"

class Player : public GameObject {
public:
	float moveSpeed = 0.5f;

	std::vector<Missile> playerMissiles;
	std::vector<Life> playerHealth;

	int Update(float dt, Bindings keyBinds, std::vector<Missile> & alienMissileList, int &hit);

	void fireWeapon();

	Player::Player() :GameObject()
	{
		//Creating player health icons
		for (int i = 0; i <= 2; i++) {
			Life newLife;
			newLife.place(i);
			playerHealth.push_back(newLife);
		}
	}
};