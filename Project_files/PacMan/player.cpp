
#include "player.h"

int Player::Update(float dt, Bindings keyBinds, std::vector<Missile> & alienMissileList, int &hit) {

	//Disables the player if they have no health left
	if (playerHealth.size() > 0) {
		

		//Player movement
		if (keyBinds.leftPressed) {
			if (modelMatrix[3].x > -0.8f) {
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-moveSpeed * dt, 0.0f, 0.0f));
			}
		}
		if (keyBinds.rightPressed) {
			if (modelMatrix[3].x < 0.8f) {
				modelMatrix = glm::translate(modelMatrix, glm::vec3(moveSpeed * dt, 0.0f, 0.0f));
			}
		}

		if (keyBinds.firePressed) {
			fireWeapon();
		}


		//Detects if it has been hit by an alien missile
		for (int i = 0; i < alienMissileList.size(); i++) {
			if ((fabs(alienMissileList[i].modelMatrix[3].x - modelMatrix[3].x) < 0.08) && (fabs(alienMissileList[i].modelMatrix[3].y - modelMatrix[3].y) < 0.08)) {
				alienMissileList.erase(alienMissileList.begin() + i);
				playerHealth.erase(playerHealth.begin());
				hit = 5;
			}
		}
	}
	

	return -1;
}

void Player::fireWeapon() {

	//Allows only a single missile on screen at any time
	if (playerMissiles.size() < 1) {

		//Spawns a new missile and cretaes its geometry
		Missile newMissile;

		newMissile.loadModel("Models/Bullet.dae");

		/*newMissile.vertices.assign({
			0.02f , 0.02f , 0.0f,		1.0f, 1.0f,
			0.02f , -0.02f , 0.0f,		1.0f, 0.0f,
			-0.02f , -0.02f , 0.0f,		0.0f, 0.0f,
			-0.02f , 0.02f , 0.0f,		0.0f, 1.0f });
		newMissile.indices.assign({
			0,1,2,
			0,3,2 });*/

		//Places the missile at the players location
		newMissile.modelMatrix = modelMatrix;

		newMissile.SetupImg("Models/PlayerMissile.png");
		newMissile.Setup();

		//Adds missile to the player missile list
		playerMissiles.push_back(newMissile);
	}
	

	return;
}

//std::vector<std::shared_ptr<Player>> Player::playerList;